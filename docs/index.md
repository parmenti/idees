# *Informatique : Didactique et EnseignementS* (IDEES)

<br/>

## Bienvenue sur la page du groupe IDEES

IDEES est un groupe de réflexion et de travail hébergé par le laboratoire [Loria](https://www.loria.fr) et dédié à **l'enseignement et la didactique de l'informatique**. 
L'introduction de l'enseignement de l'Informatique dans le second degré à la rentrée 2012 dans le cadre de la [spécialité « Informatique et Sciences du Numérique » (ISN) en terminale S](https://media.eduscol.education.fr/file/consultation/88/1/terminale_projet_prog_2011_ISN_S_170881.pdf), puis la mise en place à la rentrée 2019 de la [discipline « Numérique et Sciences Informatiques » (NSI) en 1ère](https://eduscol.education.fr/document/30007/download) et en [terminale](https://eduscol.education.fr/document/30010/download) de l'enseignement général, accompagnée par la mise en place d'un enseignement intitulé [« Sciences Numériques et Technologie » (SNT) à destination de tous les élèves de 2nde](https://eduscol.education.fr/1670/programmes-et-ressources-en-sciences-numeriques-et-technologie-voie-gt), ont fait émerger de nombreuses questions sur la didactique de l'Informatique. Même si certains d'entre nous ont participé à la formation à l'Informatique des enseignants du second degré, ces questions se posent à tous les niveaux de l'enseignement de l'informatique à l'université.

<br/>

## Rejoindre le groupe

Si vous souhaitez participer ou plus simplement suivre les actualités du groupe, il vous suffit de vous abonner à la liste de diffusion <tt>nsi-snt-nancy-metz@loria.fr</tt> via le [lien suivant](https://sympa.inria.fr/sympa/info/nsi-snt-nancy-metz). N'hésitez pas à y envoyer un bref message pour vous présenter. Notez qu'il n'est pas nécessaire d'être membre du Loria pour faire partie du groupe, et nous sommes heureux d'accueillir toutes les personnes intéressées par l'enseignement du numérique et des sciences informatiques.

<br/>

## Événements


::timeline::

- content: "Le programme de la journée est disponible [ici](journees/2024.md)."
  icon: ''
  sub_title: "2024-04-03"
  title: "Journée académique NSI-SNT 2024"

- content: "Le programme de la journée est disponible [ici](journees/2023.md)."
  icon: ''
  sub_title: "2023-04-05"
  title: "Journée académique NSI-SNT 2023"

- content: "Le programme de la journée est disponible [ici](journees/2022.md)."
  icon: ''
  sub_title: "2022-04-27"
  title: "Journée académique NSI-SNT 2022"

- content: "Le programme de la journée est disponible [ici](journees/2020.md)."
  icon: ''
  sub_title: "2020-03-26"
  title: "Journée académique NSI-SNT 2020 (annulée en raison de la Covid)"

- content: "Le programme de la journée est disponible [ici](journees/2019.md)."
  icon: ''
  sub_title: "2019-03-07"
  title: "Journée académique ISN-EPI 2019"

- content: "Le programme de la journée est disponible [ici](journees/2018.md)."
  icon: ''
  sub_title: "2018-03-22"
  title: "Journée académique ISN-EPI 2018"

- content: "Le programme de la journée est disponible [ici](journees/2017.md)."
  icon: ''
  sub_title: "2017-03-30"
  title: "Journée académique ISN-EPI 2017"

- content: "Le programme de la journée est disponible [ici](journees/2016.md)."
  icon: ''
  sub_title: "2016-03-17"
  title: "Journée académique ISN-EPI 2016"

- content: "Le programme de la journée est disponible [ici](journees/2015.md)."
  icon: ''
  sub_title: "2015-03-12"
  title: "Journée académique ISN-EPI 2015"

- content: "Le programme de la journée est disponible [ici](journees/2014.md)."
  icon: ''
  sub_title: "2014-04-17"
  title: "Journée académique ISN-EPI 2014"

- content: "Le programme de la journée est disponible [ici](journees/2013.md)."
  icon: ''
  sub_title: "2013-06-27"
  title: "Journée académique ISN-EPI 2013"

- content: "Journées organisées en partenariat avec la SIF et la DGESCO sur la formation ISN regroupant les acteurs de cette formation. Les documents de ces journées sont disponibles [ici](journees/sif2013.md)."
  icon: ''
  sub_title: "2013-04-11/12"
  title: "Journées SIF et ISN"
  
- content: "Séminaire donné par Richard Hotte ([plus d'informations](seminaires/hotte.md))"
  icon: ''
  sub_title: "2013-01-14"
  title: "Séminaire «Ingénierie d’un système d’apprentissage en ligne»"
  
- content: "Séminaire donné par Franck Varenne ([plus d'informations](seminaires/varenne.md))"
  icon: ""
  sub_title: "2012-10-22"
  title: "Séminaire «Epistémologie de l'informatique et applications»"
  
- content: "Lecture de l'article *Teaching Programming in Secondary School: A Pedagogical Content Knowledge Perspective* accessible [en ligne](https://files.eric.ed.gov/fulltext/EJ1064282.pdf)."
  icon: ""
  sub_title: "2012-03-26"
  title: "Groupe de lecture animé par Martin Quinson"

::/timeline::

## Soutiens

<br/>
En plus de l'implication d'enseignant.e.s et d'enseignant.e.s chercheur.euse.s, le groupe IDEES bénéficie du soutien de diverses institutions et personnes :

<br/>

::cards:: cols=3

- title: Loria
  content: "Le Loria (laboratoire d'informatique de l'Université de Lorraine, UMR CNRS 7503), via son directeur Jean-Yves Marion, soutient les actions réalisées par le groupe IDEES"
  image: ../img/logos/loria.png
  url: "https://www.loria.fr"

- title: "Marie Baron"
  content: "Marie apporte son expertise en communication pour l'ensemble des activités du groupe, notamment l'organisation des journées SNT-NSI"
  image: ../img/icons/003-athena.png

- title: "Rectorat de l'académie de Nancy-Metz"
  content: "Le Rectorat, via Mohammed Hayouni, Inspecteur d'Académie - Inspecteur Pédagogique Régional de Mathématiques en charge de la discipline NSI dans l'académie de Nancy-Metz, apporte son soutien aux journées SNT-NSI, notamment via leur inscription au plan académique de formation."
  image: ../img/logos/logo_academie_nancy.png
  url: "https://www.ac-nancy-metz.fr/"

::/cards::

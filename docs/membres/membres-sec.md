## Enseignant.e.s du secondaire

<br/>

::cards::

- title: Manuel Bricard
  content: Enseignant au lycée Loritz de Nancy.
  image: ../img/icons/001-zeus.png

- title: Martin Canals
  content: Enseignant au lycée Loritz de Nancy.
  image: ../img/icons/006-ares.png

- title: Abdellatif Kbida
  content: Enseignant au lycée Bichat de Lunéville.
  image: ../img/icons/007-poseidon.png

- title: Christophe Maury
  content: Enseignant au lycée Jean Hanzelet de Pont-à-Mousson.
  image: ../img/icons/008-atlas.png

- title: Jérôme Metzler
  content: Enseignant au lycée Vauban de Luxembourg.
  image: ../img/icons/005-hephaestus.png

::/cards::

<small>Icones de la mythologie grecque réalisées par [max.icons](https://www.flaticon.com/authors/maxicons) et disponible sur [www.flaticon.com](https://www.flaticon.com).</small>

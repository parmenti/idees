## Journée SNT-NSI

!!! tip ""
	Jeudi 26 mars 2020 (journée annulée à cause de la Covid)<br/>
	Loria, Nancy<br/>
	Interventions, exposés, conférences ponctueront cette journée ainsi que des ateliers, visant à approfondir certaines thématiques liées au numérique.

## Programme

* 9h00-9h15 : Accueil et présentation de la journée

* 9h15-11h : **Ateliers - Session 1**

* 11h00-11h30 : Pause café

* 11h30-12h45 : **"Réintégrer les femmes dans les métiers du numérique : études de pratiques inclusives"**, par Isabelle Collet, Maîtresse d'enseignement et de recherche en sciences de l’éducation à l'Université de Genève.

!!! tip "Résumé"
	L’informatique joue un rôle croissant dans l’évolution de nos sociétés, mais les femmes sont largement sous-représentées dans ces métiers depuis plusieurs décennies. Pourtant, dans les années 80, l’informatique était un métier plutôt féminisé, du moins, pour un métier technique. Aujourd’hui, en ce qui concerne les métiers au coeur de la tech, les femmes sont moins de 15%. La figure du geek sert souvent de bouc émissaire dans le sens où elle serait porteuse des représentations qui amèneraient les femmes à s’autocensurer. Or, bien plus qu’une autocensure, il s’agit d’une censure sociale qui écarte les filles et les femmes des sciences et techniques, et plus particulièrement de l’informatique. Face à un phénomène systémique, certaines universités ont obtenu de façon pérenne une représentation quasi paritaire dans les filières informatiques. L’objectif de cette intervention est de réfléchir aux pratiques incitatives efficaces et à leurs fondements théoriques, afin de penser l’inclusion des femmes dans le monde professionnel de l’informatique comme un changement de culture et de pratiques. 
	
!!! tip "À propos de l'oratrice"
	Isabelle Collet est informaticienne scientifique de formation. Elle a publié en 2019 « Les oubliées du numérique » aux Editions Le Passeur. Elle travaille sur la question du genre dans les sciences et techniques et également sur la manière de pratiquer une véritable pédagogie de l’égalité à l’école.

* 12h45-13h00 : **"Présentation du projet '1 scientifique - 1 classe : Chiche'"**, par Erwan Kerrien, chercheur, et Véronique Poirel, Chargée de médiation scientifique, Inria.

* 13h-14h : Pause déjeuner

* 14h-15h30 : **Ateliers - Session 2**

* 15h30-16h0 : Pause café

* 16h00-16h20 : **" Sciences Numériques et Technologie (SNT) et Numérique et Sciences Informatiques (NSI), point sur l'académie de Nancy-Metz"**, par Mohammed Hayouni, IPR.

* 16h20-17h00 : **"Table ronde sur les formations informatiques postbac à l'Université de Lorraine et sur les pré-requis attendus à la sortie de terminale"**, présence d'enseignants du BUT informatique et MMI de Nancy-Charlemagne, de la licence informatique et double licence de la FST, de la licence MIASHS de l'IDMC.

* 17h00 : Fin de la journée

## Exposition 'De l'homo numericus au citoyen numérique'

Possibilité pendant les temps libres de cette journée de visiter au Loria cette exposition.

!!! tip "Description"
	Maîtriser le numérique pour ne pas le subir, c’est ce que propose cette exposition itinérante. Au travers d’animations ludiques, elle aide le visiteur à se familiariser avec les concepts sur lesquels repose l’informatique (les informations, les langages, les algorithmes et les machines) et le sensibilise aux enjeux du numérique, pour l’aider à devenir un citoyen éclairé sur ces questions.


## Descriptifs des ateliers

!!! tip "Atelier 1 : Blockly : prise en main de l'API - David LANGLOIS - session 1"
	Blockly est un ensemble de fonctionnalités javascript (API) permettant de manipuler des blocs graphiques pour rédiger des algorithmes. Les algorithmes peuvent alors être exécutés pour modifier un "monde" (par exemple: dépacer un robot dans un labyrinthe). Plusieurs applications web basées sur blockly existent (comme [https://blockly-games.appspot.com/turtle?lang=fr](https://blockly-games.appspot.com/turtle?lang=fr)), qui permettent de découvrir les bases de l'algorithmique en résolvant des défis. L'atelier consiste à découvrir l'API afin de créer ses propres applications. Au cours de l'atelier, le lien avec les programmes informatiques du lycée sera fait. Notamment, on s'intéressera à l'articulation entre l'univers fermé des blocs graphiques de scratch, et celui ouvert de blockly. On verra en quoi derrière blockly se cache un langage de programmation écrit, du XML, du javascript. Blockly peut-il être utilisé comme un retour aux sources (le scratch du collège) avec une prise de recul suite à la formation du lycée? L'atelier est destiné à des participants maîtrisant les bases de javascript.

!!! tip "Atelier 2 : SNT : robot Micro-Bit et activités Python, échanges autour de la progression - Jérome METZLER - session 1 et/ou 2"
	L'atelier devrait être présenté en trois grandes parties. Une première partie, axée sur le thème "Informatique embarquée et objets connectés", avec assemblage puis programmation d'un petit robot imprimé en 3D, construit autour d'un Micro-Bit et piloté lui-même par un autre Micro-Bit. Une deuxième partie, à la thématique plus large et ouverte sur les six autres thèmes de l'enseignement de seconde, avec présentation de six activités relativement simples de programmation Python à faire avec les élèves en classe. Une troisième partie, avec une ouverture plus générale sur l'ensemble du programme, où une idée de progression réalisée sera présentée, et surtout où les échanges et les discussions devraient permettre de pouvoir améliorer les progressions de chacun d'entre nous.

!!! tip "Atelier 3 : Moteurs physiques, jeux vidéos et simulateurs de comportement - Vincent THOMAS - session 1 (ou 2)"
	De nombreuses applications numériques utilisent des moteurs physiques que ce soit pour avoir des réactions réalistes dans des jeux (déplacements dans Super Mario, simulation de l'inertie du joueur dans les FPS, ...), pour proposer des simulateurs (pédagogiques comme algodoo ou professionnels) ou pour aider le développement d'animations 3D (moteur dans Blender, ...). Cet atelier cherchera à faire le lien entre la physique et l'informatique. (1) Dans un premier temps, nous écrirons ensemble un moteur physique très simple constitué de quelques instructions (en Python avec la librairie pygame) et basé sur la mécanique du point (système masse-ressort). (2) Nous verrons ensuite comment l'étendre pour en faire un moteur de jeu vidéo. (3) Puis, nous tirerons parti des lois de la physique pour aborder le domaine de l'Intelligence artificielle et simuler des comportements de foule inspirés de la biologie (boids et flocking) et utilisés dans de nombreux domaines (Dimensionnement de bâtiments, Effets spéciaux, ...). <br/>Liens avec NSI : projets étudiant (création de jeux vidéos temps réel [affichage, lois d'évolution, ...], simulation de phénomènes physiques de première/terminale [chute des corps, système ressorts, ...], Terminale : modélisation orientée objets (mais pas central à l'atelier). Page web de l'atelier : [https://members.loria.fr/VThomas/mediation/ISN_moteur_2017/](https://members.loria.fr/VThomas/mediation/ISN_moteur_2017/) (on reprendra ces éléments ensemble lors de l'atelier)

!!! tip "Atelier 4 : Labyrinthe et recherche de chemins - Vincent THOMAS - session 2 (ou 1)"
	Les labyrinthes sont intéressants à étudier car ils constituent des cas typiques de problème de prise de décision dans lesquels un agent intelligent doit savoir raisonner sur le long terme: en effet, pour décider quel couloir emprunter, il faut déjà savoir ce qu'il sera possible de faire au bout des différents couloirs. Au cours de cet atelier, on programmera ensemble (en python) l'algorithme de Lee, un algorithme très simple permettant de construire le chemin le plus court dans un labyrinthe. En fonction du temps et des envies, on pourra regarder les principes de l'algorithme classique A* qui permet d'améliorer grandement la recherche en utilisant une connaissance du problème et/ou étendre ces algorithmes pour construire des agents intelligents capable de raisonner sur le long terme dans des contextes divers (jeux, robots, ...). <br/>Lien avec NSI : projets étudiant (l'IA dans les jeux, planification et recherche de chemin) ; Première: représentation des données, parcours de tableaux, algorithme glouton ; Terminale: structures de données (graphe et pile), algorithmique (récursivité, programmation dynamique). Page web de l'atelier : [https://members.loria.fr/VThomas/mediation/ISN_2018_labyrinthe/](https://members.loria.fr/VThomas/mediation/ISN_2018_labyrinthe/) (on reprendra ces éléments ensemble lors de l'atelier)

!!! tip "Atelier 5 : Programmer en python côté serveur, un outil au service du programme. - Manuel BRICARD - session 1 et/ou 2"
	L'atelier n'est pas la répétition de celui proposé l'année dernière. Cette année j'essaierai de montrer, à travers des exemples, comment il est possible d'utiliser la programmation côté serveur comme un fil rouge permettant d'aborder diverses parties du programme, généralement sous forme de projets/DM.

!!! tip "Atelier 6 : Reconstruction d'images, tomographie. - Erwan KERRIEN - session 2"
	L'imagerie médicale est un des ressorts les plus importants des progrès médicaux de ces dernières décennies, et pourtant l'essentiel des images produites sont des images reconstruites, c'est-à-dire générées par un algorithme. Dans cet atelier, nous débuterons par une activité ludique sans ordinateur, pour nous interroger sur la possibilité (ou pas) de reconstituer une image à partir de mesures. Ce jeu, inspiré de la bataille navale, reprend les bases de la reconstruction tomographique permettant d'obtenir notamment des images scanner. Puis nous discuterons des difficultés rencontrées et passerons en revue brièvement les algorithmes de tomographie les plus courants. Cet atelier est en lien avec le thème "Photographie numérique" de l'enseignement SNT.

!!! tip "Atelier 7 : Les données personnelles ou comment payer ses services numériques - Yannick PARMENTIER - session 2"
	Dans cet atelier, nous proposons de recourir à un jeu de rôles pour sensibiliser les élèves au concept de données personnelles, afin qu'il.elle.s en saisissent les enjeux (utilisations et besoin de protection notamment). Pour cela, nous utiliserons les ressources proposées par l'association l'Arbre Des Connaissances, notamment leur activité en lien avec le Règlement Général de Protection des Données de l'Union Européenne. Si le temps le permet, nous poursuivrons sur un jeu en ligne permettant d'illustrer les "fuites" de données personnelles sur les réseaux sociaux. Pour finir, nous proposerons une activité de programmation pouvant être utilisée comme support à la définition du concept de donnée personnelle, et basée sur les intégrammes. Le contenu de cet atelier pourrait être exploité dans le cadre de l'enseignement sur les "Réseaux Sociaux" en SNT.

!!! tip "Atelier 8 : Dessin vectoriel et Programmation Web - Laurent DUPONT - session 1 ou/et 2"
	Les images utilisées en informatique, notamment sur le web, sont essentiellement de deux types, l'un dit "bitmap", l'autre dit "vectoriel". Les deux se caractérisent par la manière dont l'information est stockée. Pour le premier, la couleur (et implicitement la position) de chaque point de l'image est stocké ; pour le deuxième, ce sont les formes géométriques dessinées dans l'image (et leur couleur) qui sont stockées. Le premier type est très adapté pour les photographies et se décline sous de nombreux formats dont les plus connus sont jpeg, png, bmp. Le deuxième type est lui adapté aux logos et aux images "similaires" ; il se décline notamment sous les formats ai, svg, pdf. Dans la programmation web actuelle, le format vectoriel est de plus en plus utilisé pour ses propriétés de taille (réduite) et de mise à l'échelle (zoom sans effet de pixelisation). De plus, l'animation de telles images, la gestion des évènements associés, peut se faire aisément en javascript en manipulant des formes géométriques, ou directement en CSS puisque le fichier svg s'intègre au DOM (Document Object Model) contenant la structure de la page HTML. L'exploitation des fichiers svg permettra d'illustrer la structure d'arbre (format d'enregistrement des données - Terminale), la programmation web (2e -1ere) en abordant les interactions entre html, css, javascript et svg. Cela pourra être l'occasion de proposer un projet exploitant ces différents aspects du programme. Prérequis : quelques connaissances en html/css/javascript.

!!! tip "Atelier 9 : Les mots de Maupassant - Abdellatif KBIDA - session 1 et/ou 2"
	Dans le cadre de cet atelier nous évoquerons un domaine particulier de l'IA : le machine learning ou apprentissage automatique. À partir d'un corpus de textes de Maupassant, l'objectif est d'écrire un script Python capable de produire de courtes phrases "intelligibles" à la "manière" de Maupassant. Lors de la première phase d'apprentissage non-supervisé des items des programmes de SNT et NSI seront exploités tels que : Structure de données (en particulier les dictionnaires), Recherche dans des données structurées ou non et mise en forme des informations produites, Importation d'une table depuis un fichier csv, json ...  Lors de la deuxième phase, celle d'un apprentissage supervisé, la mise en place d'une interface homme machine web développée en Python/html/css sera présentée. Il est possible d'accompagner ce projet de recherches, d'exposés et de débat autour de l'intelligence artificielle afin de démystifier de fausses idées que pourraient avoir les élèves sur ce sujet. Ce projet a été en partie expérimenté et développé lors de la dernière session de TPE 2018/2019.

!!! tip "Atelier 10 : Dessine moi des pixels ! - Sylvain CONTASSOT - session 1"
	Nous décrirons les deux grands types d'images numériques et leurs propriétés mathématiques ainsi que les représentations des couleurs. Ensuite, nous nous pencherons sur les algorithmes pour tracer des objets géométriques simples tels que des rectangles pleins, des segments quelconques, des disques et des cercles. Enfin, si le temps nous le permet, nous aborderons la notion de lissage ou d'anti-crénelage (anti-aliasing). Cet atelier est en lien avec la partie algorithmique et projet du programme de NSI.

!!! tip "Atelier 11 : IA et lapins ... - Maxime AMBLARD - session 2"
	L’informatique est loin de se faire uniquement avec des ordinateurs. Cette activité inspirée de problèmes d’IA est basée sur des lapins, des formules magiques et des carottes. Elle met en avant les notions de grammaires formelles et de systèmes de réécriture. Nous montrerons que les lapins sont le chainon manquant entre informatique et linguistique ! Cette mise en situation montre comment des algorithmes dynamiques permettent de reconnaître des phrases du français. Lien avec le programme de NSI : histoire de l’informatique : liens entre informatique et langage ; structure de données : structure d’arbre ; algorithme dynamique ; thématique pouvant être utilisée pour les projets vers l’interdisciplinarité (français). 

!!! tip "Atelier 12 : Finir chaque thème de SNT par une énigme. - Christophe MAURY - session 1 ou/et 2"
	A la fin de chaque thème, mobiliser les connaissances, les techniques et éventuellement des bouts de codes afin de résoudre une énigme. Construire une séquence d'une heure trente, pouvant également servir à l'évaluation, avec une pointe de recherche, une touche d'escape-game et l'encodage et le traitement des données. L'atelier proposera de traiter deux énigmes : Une sur le thème "Données structurées" et Une sur le thème "Photographie numérique", mais permettra un espace d'échange où seront proposées des pistes pour les autres thèmes.

!!! tip "Atelier 13 : Résoudre une enquête de police avec des bases de données - Florian DELCONTE - session 1 et/ou 2"
	Cet atelier illustrera, à l'aide de deux activités, l'une branchée et l'autre débranchée, l'intérêt et le fonctionnement des bases de données. Elles sont partout et permettent, en structurant les données, d'y accéder de manière rapide et efficace. Dans un premier temps nous pourrons toucher des bases de données et les manipuler pour les interroger. Nous verrons ensuite comment présenter sur un exemple ludique les bases de données et un langage de requêtes. Cet atelier est en lien avec le thème « les données structurées et leur traitement » de SNT

!!! tip "Atelier 14 : Turing Tumble - Phuc NGO et Nazim FATES - session 2"
	L'objectif de cet atelier est de présenter un jeu mécanique à visée pédagogique ([https://www.turingtumble.com/](https://www.turingtumble.com/)). Il s'agit d'un jeu de construction simulant un ordinateur mécanique alimenté par des billes pour résoudre des énigmes logiques. De nombreuses activités en lien avec des concepts de la programmation et la notion de machine de Turing ont été conçues et sont disponibles dans un manuel en ligne. Explorer ensemble quelques-unes de ces activités sera l'occasion d'échanger sur les possibilités de mise en œuvre en classe et les liens avec les programmes de SNT et NSI. 

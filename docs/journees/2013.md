## Journée ISN-EPI

!!! tip ""
	Jeudi 27 juin 2013<br/>
	Loria, Nancy <br/>
	La journée se découpe en conférences plénières, suivies d'ateliers pratiques.

## Programme

Les conférences ont été filmées et les vidéos sont en ligne à l'adresse [http://videos.univ-lorraine.fr/index.php?act=view&id_col=34](http://videos.univ-lorraine.fr/index.php?act=view&id_col=34)

* 9h-10h : **"Une introduction à l’intelligence artificielle"**, Jean-Paul HATON - LORIA – Université de Lorraine, Institut Universitaire de France

!!! tip "Résumé"
	L'intelligence artificielle (IA) s'attache à résoudre des problèmes qui relèvent d'activités humaines de nature variée : perception, prise de décision, planification, diagnostic, interprétation de données, compréhension du langage, conception. L’IA aborde un vaste champ d’activités que l’on peut classer en quelques grands domaines : la reconnaissance et l’interprétation de formes et de données, la planification d’actions et la robotique, le diagnostic et l’aide à la décision, le traitement de la langue naturelle, écrite et parlée, la formation assistée. Des exemples pratiques issus de ces domaines illustreront l'exposé qui sera centré sur un ensemble de défis posés à l’IA.

* 10h-10h30 : **"Grains logiciels 3.0 : du code au numérique et vice-versa"**, Florian DUFOUR - Inria Grenoble Rhône-Alpes

!!! tip "Résumé"
	Notre rêve : que tout le monde puisse manipuler des objets numériques. Notre méthode : démystifier ces objets en les développant à capot ouvert, avec des outils massivement disponibles, pour que chacun puisse se les approprier, les modifier, s'y appuyer pour produire de la pédagogie et du sens.

* 10h30-11h : Pause café

* 11h-12h : **"Conduite de projet de développement de logiciels"**, François CHAROY - LORIA - Université de Lorraine

!!! tip "Résumé"
	Ces dernières années ont vu l'apparition de nouveau processus de développement dit agiles, adaptés à la production de logiciels modernes. Ces processus mettent en avant des pratiques qui favorisent le travail collectif, les tests et la livraison fréquente de logiciel fini. Ceci permet de garantir un ajustement continu du projet au besoin de l'utilisateur tout en assurant un bon niveau de qualité. Certaines pratiques de ces méthodes peuvent être adaptées à une classe pour permettre à des groupes d'étudiants de mener un projet de façon organisée et régulière. Cet exposé a pour but d'expliquer les principes des méthodes agiles et comment conduire des projets basés sur ces pratiques dans une classe,

* 12h-12h30 : **"Jeu de société et intelligence artificielle"**, Vincent THOMAS - LORIA - Université de Lorraine

!!! tip "Résumé"
	De nombreux jeux de société parviennent à captiver les joueurs en les mettant face à des situations complexes où ils doivent faire preuve de stratégies à long terme pour l'emporter sur leurs adversaires. C'est justement parce qu'ils impliquent d'élaborer des raisonnements qu'on attribue habituellement à l'être humain, que les jeux posent des problèmes qui présentent naturellement des liens fort avec l'Intelligence Artificielle. Cette présentation cherchera à exposer rapidement différentes classes de jeu, des approches simples pour les résoudre et leur liens avec la recherche actuelle en intelligence artificielle. Elle cherchera aussi à montrer que les jeux peuvent être des points d'entrée intéressants pour proposer des projets faciles d'accés et motivants dans des enseignements en informatique. Le sujet et le corrigé d'un travail pratique sur le sujet

* 12h30-14h : Pause déjeuner

* 14h-15h: **"Petite histoire de la cybercriminalité"**, Nicolas ROUGIER - LORIA - INRIA

!!! tip "Résumé"
	Depuis les heures glorieuses du "phreaking" où des adolescents cherchaient simplement à téléphoner gratuitement (1960), jusqu'à la mise au point par des états de virus informatiques pour des attaques ciblées (2010), le visage de la cybercriminalité s'est radicalement transformé en l'espace de quelques années. Là où l'on trouvait des passionés d'informatique, on trouve aujourd'hui des états, des industries, des mafias et des "script kiddies". Les possibilités de piratage et de fraude se sont par ailleurs multipliées depuis la généralisation de l'accès à internet et de la téléphonie mobile. Sachant que le point d'entrée privilégié dans un système est l'utilisateur lambda, il est plus que jamais nécessaire de rester vigilant.

* 15h-16h : **Ateliers - Session 1 : ateliers 1, 2, 3, 8, 9**

* 16h-16h30 : Pause café

* 16h30-17h30 : **Ateliers - Session 2 : ateliers 1, 4 , 5, 6, 7, 10**

## Descriptifs des ateliers

Faites votre choix au moment de votre inscription, mais tenez compte de la répartition des ateliers dans les deux sessions (cf. programme ci-dessus).

!!! tip "Atelier 1 : La cryptographie dans tous ses états - Marie DUFLOT-KREMER et Stéphane GLONDU"
	Sécuriser les transactions bancaires, protéger des messages, assurer la confidentialité des échanges sont quelques unes des nombreuses applications de la cryptographie. A vous de créer ou d'attaquer des protocoles de sécurité !

!!! tip "Atelier 2 : Physical Computing, Robotique et Art interactif : la programmation sous un autre regard -Laurent CIARLETTA"
	Lorsque des étudiants commencent l'apprentissage de la programmation, les premières réalisations peuvent paraître aux non-initiés bien souvent abstraites, désincarnées ou trop austères. Cela rend parfois ingrats les apprentissages dans nos domaines. Or, la conception de programmes, d'algorithmes et la programmation en général sont des activités qui nécessitent non seulement d'avoir une bonne technique et de maîtriser des concepts de base mais également d'être créatifs ou inventifs. Il existe un certain nombre de plateformes et d'environnements de développement qui permettent soit par une approche visuelle, interactive et artistique, soit par un retour vers le monde physique de mettre l'apprentissage de la programmation à la portée du plus grand nombre. En se basant sur des expériences d'enseignement aux Mines de Nancy et dans ARTEM, cet atelier montrera trois grandes classes d'outils pour monter des cours et des projets qui rendront tangibles les efforts mis par les étudiants dans leurs apprentissages : l'environnement et le langage Processing, la plateforme Arduino, pour le Physical Computing, et les challenges informatiques liés à la robotique (depuis des robots simple jusqu'à des drones).

!!! tip "Atelier 3 : Informatique débranchée - Martin QUINSON"
	Contrairement à ce que beaucoup de monde pense, les ordinateurs ne sont pas la seule raison d'être de l'informatique. Pour preuve, cet atelier présente diverses activités à faire avec des pions, des jetons ou des bouts de bois, mais sans aucun ordinateur et même sans électricité. Pourtant, ces petits jeux permettront à chacun de découvrir de manière ludique les notions au cœur de l'informatique : ce qu'est un algorithme et ce qui fait qu'un algorithme est meilleur qu'un autre, ou encore comment coder et transmettre une information.

!!! tip "Atelier 4 : Apprendre par imitation : du bon usage du tutoriel - Abdellatif KBIDA et Christophe THOMAS"
	Cette première année d'enseignement de spécialité informatique et sciences du numérique fut extrêmement riche. Tant au niveau des contenus enseignés que des méthodes et de la pédagogie déployées, parmi celles-ci nous avons régulièrement utilisé des tutoriels en ligne. L'enseignement de certaines technologies informatiques se prête particulièrement bien à l'usage de tutoriels. Dans cet atelier, nous tenterons de réfléchir sur plusieurs points tels que : Qu'est-ce qui caractérise un "bon" tuto (et comment faire un bon tuto) ? Comment exploiter au mieux un tuto ? Comment aider les élèves à s'approprier le contenu d'un tuto ? Les limites de l'utilisation des tutos.

!!! tip "Atelier 5 : JLM : un exerciseur pour Java et python - Martin QUINSON"
	L'entrainement et l'exercice jouent un rôle prédominant dans l'apprentissage de la programmation, mais cette activité est difficile à encadrer en tant qu'enseignant avec l'approche traditionnelle. JLM (Java Learning Machine) est un exerciseur automatisé permettant aux élèves d'apprendre la programmation en Java ou python à leur rythme. L'aspect ludique de cet outil motive les élèves à résoudre les quelque 150 problèmes proposés. Ils acquièrent ainsi la syntaxe du langage utilisé (Java ou python), ainsi que leurs premiers réflexes algorithmiques classiques (boucle de recherche, récursivité, tris, etc). Nous verrons les fonctionnalités de cet outil, tant du point de vue de l'élève (pour la résolution de problème) que de celui du professeur (pour la modification de séquences de problèmes ou la rédaction de nouveaux problèmes dans l'outil). Pour pleinement profiter de cet atelier, les participants devraient avoir un ordinateur portable avec Java installé.

!!! tip "Atelier 6 : Présentation du site France-IOI en complément des cours. - Guillaume LE BLANC"
	Le site internet de l'association France IOI ([http://www.france-ioi.org/](http://www.france-ioi.org/)) présente une large base d'exercices (plus de 1000) classés par niveau et intégrés dans une progression pédagogique. Depuis la dernière reforme du programme de mathématiques, une centaine d'exercices ont été créés tout spécialement adapté à ce programme. Ils constituent le niveau 1 d'apprentissage. Et couvrent les notions d'affectation, boucle et test plus quelques compléments mathématiques. Le niveau 2 traite pour l'essentiel des fonctions, tableaux à une dimension et chaînes de caractères. Le niveau 3 aborde les notions de tri, recherche, récursivité et bases. Les niveaux supérieurs portent sur d'autres notions au programme de l'ISN comme le parcours en profondeur et les graphes ou comme des algorithmes géométriques ou numériques sans doute utiles en classe préparatoires aux grandes écoles. Les exercices peuvent être actuellement validés en Python, Java, Java's Cool, OCaml, C, C++ et Pascal. Nous présenterons la structure du site, la progression pédagogique qu'il propose et les outils de gestion de groupes. Nous proposerons également une façon de l'utiliser expérimentée en ISN en complément des cours.

!!! tip "Atelier 7 : Des dés pour coder : Sylvain LEFEBVRE et Erwan KERRIEN"
	Que serait internet sans image, sans musique, sans film ? Ces divers médias sont aisément accessibles parce qu'ils sont compressés. Cette activité propose une introduction à la compression de données, en découvrant par le jeu diverses manières de transmettre une image.

!!! tip "Atelier 8 : Enigma : présentation et éléments de cryptanalyse - Jérémie DETREY"
	La machine Enigma, permettant de chiffrer et déchiffrer des messages de manière réputée inviolable, fut utilisée par l'Allemagne nazie jusqu'à la fin de la seconde guerre mondiale. Cependant, les travaux de cryptanalystes polonais puis britanniques (dont Alan Turing) permirent très tôt aux Alliés de décrypter les messages allemands à leur insu, jouant ainsi un rôle importante dans l'issue du conflit en Europe. Lors de cet atelier, nous regarderons cette machine et son système de chiffrement de plus près, puis nous esquisserons les principales idées qui ont permis d'en décrypter les messages.

!!! tip "Atelier 9 : L'atelier artEoz - Martine GAUTIER et Brigitte WROBEL-DAUTCOURT"
	L'atelier artEoz vous propose la prise en main d'un logiciel permettant de visualiser l'exécution d'un programme écrit en Java ou en Python. Cette plateforme s'adresse en premier lieu à des débutants en programmation ; elle permet de comprendre les mécanismes de base comme l'affectation ou l'instanciation, à travers une représentation schématique de la mémoire. De façon générale, cet outil permet également de visualiser des mécanismes plus complexes, comme la gestion des collections, la gestion de la pile à l'exécution ou des objets morts. Cet atelier se déroule en salle machine.

!!! tip "Atelier 10 : Résolution de jeu de stratégie abstrait - Vincent THOMAS"
	En s'appuyant sur un jeu particulier (a priori le jeu de TicTacToe), cet atelier proposera aux participants de programmer en python un programme d'intelligence artificielle capable de jouer de manière optimale. L'objectif de cet atelier est de commencer de rien pour bâtir entièrement un jeu et un solveur et de montrer comment cette démarche peut s'étendre à d'autres jeux. Cliquez [ici](http://www.loria.fr/~vthomas/ISN) pour accéder à l'ensemble des fichiers.
